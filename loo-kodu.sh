#!/bin/bash
export LC_ALL=C
# Autor: Uku-Mart Uprus, 2016
# Versioon: 1.0.1
# Skript teeb järgnevat: Kontrollib kas skripti käivitaja on juurkasutaja õigustes. Uuendab repositooriume ning paigaldab Apache2 teenuse. Viib sisse muudatused /etc/hosts failis vastavalt kasutaja sisendile. Viib sisse muudatused Apache2 teenuse konfiguratsioonifailidesse /etc/apache2/sites-available/<kasutaja sisend> ja /etc/apache2/sites-enabled/<kasutaja sisend>. Teeb Apache2 teenusele restardi.


# Kontrollime kas skript on käivitatud juurkasutaja õigustes
if [ $UID -ne 0 ]
then
    echo "Käivita skript juurkasutaja õigustest!"
    exit 1
fi

# Kontrollime kas kasutaja on sisestanud veebiaadressi (parameetri), kui parameeter puudub siis kuvatakse näidis kuidas skripti korrektselt käivitada
if [ $# != 1 ]
then 
	echo "Näide kuidas käivitada: ./loo-kodu.sh www.example.com"
	exit 1
else
	SAIT=$1
fi

# Kontrollime kas apache2 server on paigaldatud, kui ei ole siis repositooriumi uuendus ja apache 2 paigaldus
type apache2 > /dev/null 2>&1
if [ $? -ne 0 ]
then
	apt-get update && apt-get install apache2
fi

# Testime kas kasutaja sisend eksisteerib /etc/hosts failis. Kui ei ole siis loome nimelahenduse /etc/hosts faili localhost aadressile

if grep "$SAIT" /etc/hosts > /dev/null
then
	echo "/etc/hosts failis on $SAIT kohta kirje olemas"
else
	echo "127.0.0.1 $SAIT" >> /etc/hosts
fi

# Testime loodava veebisaidi kataloogi olemasolu 
test -d /var/www/html/$SAIT

if [ $? = 0 ]
then	
	echo "Kaust on olemas"
else
	mkdir /var/www/html/$SAIT
fi

# Loome index.html koopia loodavale saidile 
cp /var/www/html/index.html /var/www/html/$SAIT/index.html

# Muudame /var/www/html/index.html päise vastavalt kasutaja sisendile
sed -i "11s/.*/<title>$SAIT<\/title>/" /var/www/html/$SAIT/index.html

# Loome 000-default.conf koopia loodavale saidile 
cp /etc/apache2/sites-available/000-default.conf /etc/apache2/sites-available/$SAIT.conf

# Muudame konfiguratsioonifailis Servername-i 
sed -i "s/#ServerName www.example.com/ServerName $SAIT/g" /etc/apache2/sites-available/$SAIT.conf

# Muudame konfiguratsioonifailis DocumentRoot-i
sed -i "s#DocumentRoot /var/www/html#DocumentRoot /var/www/html/$SAIT#g" /etc/apache2/sites-available/$SAIT.conf

# Viime muudatused sisse ka sites-enabled kaustas asuvasse konfiguratsioonifaili
a2ensite $SAIT

# Teenuse Apache2 restart
service apache2 restart
