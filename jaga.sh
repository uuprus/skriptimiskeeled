#!/bin/bash
export LC_ALL=C
# Autor: Uku-Mart Uprus, 2016
# Skript teeb järgnevat:
# 1) Kontrollib kas skript on käivitatud juurkasutaja õigustes
# 2) Kontrollib kas sisendparameetreid on kas 2 või 3, muudel olukordadel kuvab õige vormistuse
# 3) Kontrollib kas Samba on paigaldatud, kui ei ole siis uuendatakse repositooriumid ja paigaldatakse Samba
# 4) Kontrollib kausta olemasolu, vajadusel loob
# 5) Vajadusel loob grupi
# 6) Loob koopiafaili, kirjutab konfiguratsiooni koopiafaili, testib koopiafaili
# 7) Kirjutab konfiguratsioonifaili üle koopiafailiga
 
# 1.Kontrollime, kas skript on käivitatud juurkasutaja õigustes
if [ $UID -ne 0 ]
then
    echo "Käivita see skript root õigustes!"
    exit 1
fi
 
# 2.Sisendparameetrite kontrollimine
if [ $# != 2 ] && [ $# != 3 ]
then
  echo "Kasutamine $0 KAUST GRUPP <JAGATUD KAUST>"
  exit 1
else
    if [ $# -eq 2 ]
    then
        KAUST=$1
        GRUPP=$2
    fi
    if [ $# -eq 3 ]
    then
        KAUST=$1
        GRUPP=$2
        SHARE=$3
    fi
fi
 
# 3.Kontrollib kas samba on paigaldatud
type samba > /dev/null 2>&1
if [ $? -ne 0 ]
then
  apt-get update && apt-get install samba -y || exit 1
fi

# 4. Kausta olemasolu kontroll, vajadusel loomine
test -d $KAUST || mkdir -p $KAUST 2> /dev/null
if [ $? -ne 0 ]
then
    echo "Kausta loomine ebaõnnestus"
    exit 1
fi

# 5.Vajadusel grupi loomine
getent group $GRUPP > /dev/null || addgroup $GRUPP > /dev/null
 
# 6.Koopiafaili loomine
cp /etc/samba/smb.conf /etc/samba/smb.conf.koopia
 
#Kausta asukoht
pushd $KAUST
KAUST=$(pwd) 
popd 

#Konfiguratsiooni kirjutamine koopiafaili
cat >> /etc/samba/smb.conf.koopia << EOF
[$SHARE]
    comment=random
    path=$KAUST
    writable=yes
    valid users=@$GRUPP
    force group=$GRUPP
    browsable=yes
    create mask=0664
    directory mask=0775
EOF
 
sleep 5
 
#Koopiafaili testimine
testparm -s /etc/samba/smb.conf.koopia
if [ $? -ne 0 ]
then
    echo "Koopiafail on katki"
    exit 1
fi
 
# 7.Konfiguratsioonifaili üle kirjutamine koopiafailiga
cp /etc/samba/smb.conf.koopia /etc/samba/smb.conf
/etc/init.d/smbd reload
echo "Skript jooksutatud edukalt!"
