#!/bin/bash
export LC_ALL=C
# Autor: Uku-Mart Uprus, 2016
# Versioon: 1.0.1
# Skript teeb järgnevat:
# Kontrollib kasutaja sisendit, vajadusel kuvab korrektse sisestusviisi.
# Jooksutab tsükliga läbi kõik võimalikud 4-kohalised PIN-koodid.
# Kui kasutaja sisend klapib tsükli pool genereeritud numbriga siis 
# skript niiölda peatatakse ning luuakse kaust, mille sisse luuakse fail
# nimega "koodid.txt". Sooritatakse ka kausta olemasolu kontroll.
# Kõik PIN-koodid, mis on suuremad kui kasutaja sisend - edastatakse
# faili "koodid.txt"

# Kontrollime kasutaja sisendit - vajadusel kuvatakse korrektne sisestusviis
if [ $# == 4 ]; then 
    PIN=$1$2$3$4
    echo $PIN
    sleep 2;
else
    echo "Käivita skript järgnevalt: ./KT1.sh 1 2 3 4"
    exit 1;
fi

# Käime for-tsükliga läbi kõikvõimalikud 4-kohalised PIN-koodi numbrid
for I in $( seq -w 0000 9999 ); do
# Kontrollime kas PIN-kood klapib kasutaja sisendiga
    if [ $PIN -eq $I ]; then
# Kausta olemasolu kontroll
    test -d $PIN
        if [ $? = 0 ]; then
            echo "Kaust on juba varasemalt olemas"
        else
            mkdir $PIN
            touch $PIN/koodid.txt
        fi
    fi   
# Edastame kõik koodid mis on suuremad kasutajasisndist faili "koodid.txt"
        echo $I 
        if [ $PIN -le $I ]; then
            echo $I >> $PIN/koodid.txt 
        fi
done

