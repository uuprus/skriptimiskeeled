#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Autor: Uku-Mart Uprus
# Versioon 1.0.1
#
# Skript otsib veebileheküljelt kindlat märksõna. Skripti käivitamisel
# peavad skript, lingid.txt ning tulemused.txt olema samas kaustas.
# Failis lingid.txt on ära kirjeldatud veebileheküljed koos otsitava
# märksõnaga nt. http://www.google.ee <otsitav märksõna>. Faili
# tulemused.txt kirjutatakse tulemus järgnevalt: http://www.google.ee
# <otsitav märksõna> JAH/EI.

import sys
import urllib2

# try-except tingimus, mis kontrollib kas skript on korrektselt käivitatud
try:
    # kasutaja sisendite omistamine muutujatele
    lingid = sys.argv[1]
    tulemused = sys.argv[2]

    # alist elemendiks on URL ja märksõna
    alist = [line.rstrip() for line in open(lingid)]
    
    # blist elemendiks on URL
    blist = [i.split(' ', 1)[0] for i in alist]
    
    # clist elemendiks on märksõna
    clist = [i.split(' ', 1)[-1] for i in alist]
    
    # URL-i ja märksõnal põhinev JAH/EI list
    tingimused = [] 

    for x in range(len(blist)):    
        
        url = blist[x]
        page = urllib2.urlopen(url)
        data = page.read()
        text_file = open(tulemused, "a")
        
        # if-else tingimusega kirjutatakse vormistatakse tulemused faili
        # tulemused.txt
        if clist[x] in data:
            text_file.write('\n')
            tingimused.append(" JAH ")
            text_file.write(url)
            text_file.write(' ')
            text_file.write(clist[x]) 
            text_file.write(tingimused[x])
        else:
            text_file.write('\n')
            tingimused.append(" EI ")
            text_file.write(url)
            text_file.write(' ')
            text_file.write(clist[x]) 
            text_file.write(tingimused[x])           
    text_file.write('\n')            
    text_file.close()    
except:
    print "KÄIVITA SKRIPT JÄRGNEVALT:"
    print "./skript.py lingid.txt tulemused.txt"
