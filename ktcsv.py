#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Autor: Uku-Mart Uprus, A21
# Versioon 1.0.1
# Skript käivitatakse järgnevalt: python kt.py <sisendfail> <väljundfail>
# Skript võimaldab sisendfailist luua csv-vormistusega väljundfaili
# Eeldused:
# Sisendfailis peab olema iga numbri ja eesnime vahel kasutatud <tab>-i
# Eesnime ja perekonnanime ning grupi vahel 1 tühik


import sys
import csv
import random
import string

# Parameetrite väärtustamine kasutaja poolt sisestatud argumentidega
try:
    sfail = sys.argv[1]
    vfail = sys.argv[2]
except:
    print "Käivita skript järgnevalt:"
    print "python kt.py sisendfail.txt <väljundfaili nimi>"
    sys.exit(0)

# Sisendfaili andmete töötlemine
try:
    # Sisendfaili viimine listi kujule
    alist = [line.rstrip() for line in open(sfail)]
    
    # Listis olevate tühjade elementide eemaldamine
    alist = filter(None, alist)

    # Esimese elemendi eemaldamine --> 'Nr	Tudeng	Kokku	Märkused'
    del alist[0]

    # Listi viimine kujule kus elemendiks on Eesnimi Perekonnanimi Grupp
    clist = []
    for blist in alist:
        clist.append(blist[2:])
    
    # List kus elemendiks on eesnimi
    enimi = [i.split(' ', 1)[0] for i in clist]

    # Vaheetapp perekonnanime listi loomiseks
    p1nimi = [i.split(' ', 1)[1] for i in clist]

    # List kus elemendiks on perekonnanimi
    pnimi = [i.split(' ', 1)[0] for i in p1nimi]

    # List kus elemendiks on grupp
    grupp = [i.split(' ', 2)[-1] for i in clist]
except:
    print "Sisendfaili töötlemisel esines viga, kontrolli faili õigsust"
    sys.exit(0)


x = 0
kasmas = []
meilmas = []
tokenmas = []

# Väljundfaili vormistamine
file = open(vfail, "a")
csvfile = csv.writer(file)

while x < len(clist):
    # Kasutajanime vormistus
    kasutaja = "%s%s" % (enimi[x][:1].lower(), pnimi[x].lower())
    
    # Meiliaadressi vormistus
    meil = "%s.%s@itcollege.ee" % (enimi[x].lower(), pnimi[x].lower())
    
    # Tokeni vormistamine staadium I ehk lisan tokenisse grupi
    tokenmas.append(grupp[x])
    
    # Tokeni vormistamine staadium II ehk lisan lisaks grupile ka suvalise koodi
    tokenmas[x] = tokenmas[x] + ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.ascii_lowercase + string.digits + '-' + '_') for _ in range(20))
    # Tokeni piiritlemine 20-ne märgiga
    tokenmas[x] = tokenmas[x][:20]
 
    # Tsükli käigus lisan kasutajanimesid kasmas-i 
    kasmas.append(kasutaja)
    
    # Tsükli käigus lisan meiliaadressid meilmas-i 
    meilmas.append(meil) 
    
    # Väljundfaili täidetakse tsükli käigus erinevate listide abiga
    data = [[kasmas[x], enimi[x], pnimi[x], meilmas[x], tokenmas[x]]]
    csvfile.writerows(data)
    x += 1
    
file.close()
