#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Autor: Uku-Mart Uprus
# Versioon 1.0.1

import sys
import urllib2
from time import strftime

try:
    aadress = sys.argv[1]
    otsingufail = sys.argv[2]
except:
    print "Sisesta aadress ja otsingufail"

#Kogu kompott
alist = [line.rstrip() for line in open(otsingufail)]

#Domeeni lõpp
blist = [i.split(',', 1)[0] for i in alist]

#Märksõnad
clist = [i.split(',', 1)[-1] for i in alist]

for x in range(len(alist)):
    address = "%s%s" %(aadress,blist[x])
    url = aadress
    page = urllib2.urlopen(url)
    data = page.read()
    
    aeg = strftime("%Y-%m-%d_%H-%M-%S")
    
    if clist[x] in data:
        print "%s,%s,%s,%s" %(address,clist[x],aeg,"OK")
    else:
        print "%s,%s,%s,%s" %(address,clist[x],aeg,"NOK") 
    
